#!/bin/bash

port="$(snapctl get port)"

echo "
config:
  core.https_address: 127.0.0.1:${port:-8443}
  core.trust_password: $(snapctl get password)"
