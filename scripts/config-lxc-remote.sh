#!/bin/bash

$SNAP/bin/lxc remote add lxd.snap 127.0.0.1 --accept-certificate --password=$(snapctl get password)
$SNAP/bin/lxc remote set-default lxd.snap
